#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1
GIT_URL=${GIT_URL:-"https://gitlab.com/redhat/centos-stream/tests/kernel/audit-testsuite"}
GIT_REF=${GIT_REF:-"main"}

rlJournalStart
    rlPhaseStartSetup
        rlShowRunningKernel
        rlIsRHEL "<9" && { yum install -y perl-tests; }
        [ "$(uname -m)" = "x86_64" ] && { yum install -y glibc.i686 glibc-devel.i686 libgcc.i686; }
        rlRun "git clone $GIT_URL"
        rlRun "pushd audit-testsuite"
        rlRun "git checkout $GIT_REF"
        rlIsRHEL "<9" && rlRun "sed -i '/backlog_wait_time_actual_reset/d' tests/Makefile"
        rlRun "sed -i '/io_uring/d' tests/Makefile"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "unset DISTRO"
        rlRun "cat /proc/self/loginuid && echo $(id -u) > /proc/self/loginuid" 0-255
        rlRun "unbuffer make test"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "make clean"
        rlRun "popd"
        rlRun "rm -rf audit-testsuite"
    rlPhaseEnd
rlJournalEnd
rlJournalPrintText
