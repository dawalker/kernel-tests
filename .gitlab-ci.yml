---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml
    ref: production

.rules:
  no-merge-train:
    if: $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"
    when: never
  mr-parent:
    if: $CI_MERGE_REQUEST_ID && $CI_PROJECT_ID == $CI_MERGE_REQUEST_PROJECT_ID
  mr-fork:
    if: $CI_MERGE_REQUEST_ID && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID
  default-branch:
    if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH =~ /^redhat/
  production-branch:
    if: $CI_PIPELINE_SOURCE == "pipeline" && $CI_COMMIT_BRANCH == "production" && $CI_PROJECT_PATH =~ /^redhat/

.rules-codeowners:
  extends: .rules
  mr-parent:
    changes: [CODEOWNERS]
  default-branch:
    changes: [CODEOWNERS]

# override the default CKI workflow rules to only run in very specific
# circumstances as forks don't have the tagged runners available
workflow:
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, mr-fork]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

default:
  tags:
    - kernel-qe-internal-runner

# explicitly fail MR pipelines running in a fork instead of the parent repo
# most likely a click on https://red.ht/GitLabSSO is needed for RH employees
insufficient-permissions:
  trigger:
    project: redhat/insufficient-permissions
    strategy: depend
  rules:
    - !reference [.rules, mr-fork]

shell check cki:
  extends: .cki_tools
  before_script:
    - "# Mock beakerlib.sh to avoid source import errors:"
    - mkdir /usr/share/beakerlib
    - touch /usr/share/beakerlib/beakerlib.sh
  script:
    - shellcheck --external-sources --format=gcc cki_*/*.sh
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

shell check runtests:
  extends: .cki_tools
  script:
    - find -name '*.sh' -exec shellcheck --severity=error -f gcc {} +
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

uname with deprecated parameter:
  extends: .cki_tools
  script:
    - make deprecated_uname
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

shell check runtests mr:
  extends: .cki_tools
  before_script:
    - "# Mock external libraries include files"
    - mkdir /usr/share/beakerlib
    - touch /usr/share/beakerlib/beakerlib.sh
    - mkdir /usr/lib/beakerlib
    - touch /usr/lib/beakerlib/beakerlib.sh
    - touch /usr/bin/rhts-environment.sh
    - touch /usr/bin/rhts_environment.sh
  script:
    - |
      # run full shellcheck on files changed by the MR (use --diff-filter=d to exclude deleted files)
      readarray -t files < <(git diff --name-only --diff-filter=d "${CI_MERGE_REQUEST_DIFF_BASE_SHA}..${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-${CI_COMMIT_SHA}}" | grep -E "\.sh$")
      error=0
      for file in "${files[@]}"; do
          # force to bash to avoid POSIX warnings
          if ! shellcheck --source-path=SCRIPTDIR --external-sources --shell bash --severity warning -f gcc $file; then
            error=1
          fi
      done
      if [ "${error}" -ne "0" ]; then
        echo "Please, fix the syntax problems as separate commit if they were not caused by your changes."
        exit 1
      fi
  rules:
    - !reference [.rules, mr-parent]

shellspec:
  extends: [.shellspec, .pipeline_images]
  variables:
    USE_BASH_ORIG: 'true'
  before_script:
    # required beakerlib functions should be added as mocked to
    # spec/support/bin/
    - "# Mock external libraries include files"
    - mkdir /usr/share/beakerlib
    - touch /usr/share/beakerlib/beakerlib.sh
    # mock restraint library
    - mkdir -p /usr/share/restraint/plugins
    - touch /usr/share/restraint/plugins/helpers
    - touch /usr/bin/rhts_environment.sh

codeowners check:
  extends: .cki_tools
  variables:
    FOOTER_CONFIG: |
      name: codeowners-check
      source_code: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/deployment_tools/gitlab_codeowners_config.py
      faq_name: CKI FAQ
      faq_url: https://cki-project.org/l/devel-faq/
      documentation_url: https://cki-project.org/l/gitlab-codeowners-config-docs
      slack_name: '#team-kernel-cki'
      slack_url: https://redhat-internal.slack.com/archives/C04KPCFGDTN
      new_issue_url: 'https://gitlab.com/cki-project/cki-tools/-/issues/new?issue[title]=kernel-tests codeowners-check issue'
  script:
    - |
      # compare CODEOWNERS
      export GITLAB_TOKENS='{"gitlab.com":"GITLAB_JOB_CODEOWNERS_KERNEL_TESTS_MR_ACCESS_TOKEN"}'
      if ! python3 -m cki.deployment_tools.gitlab_codeowners_config \
          ${CI_MERGE_REQUEST_IID:+--comment-mr-iid "${CI_MERGE_REQUEST_IID}"} \
          needs-update
      then
          python3 -m pip install git+https://gitlab.com/cki-project/kpet.git
          git clone https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git --depth 1
          python3 -m kpet --db kpet-db test list -o json > kpet-db-tests.json
          python3 -m cki.deployment_tools.gitlab_codeowners_config \
              --test-path kpet-db-tests.json \
              ${CI_MERGE_REQUEST_IID:+--comment-mr-iid "${CI_MERGE_REQUEST_IID}"} \
              diff
      fi
  rules:
    - !reference [.rules-codeowners, mr-parent]
    - !reference [.rules-codeowners, default-branch]
    - !reference [.rules-codeowners, production-branch]

check bash with mixed tabs and spaces:
  extends: .cki_tools
  script:
    - make mixed_tab_space
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

check bash lines ending with white spaces:
  extends: .cki_tools
  script:
    - make trailing_whitespace
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

check tmt lint:
  extends: .cki_tools
  before_script:
    - tmt --version
  script:
    - tmt lint --outcome-only fail .
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

check usage of internal hostnames:
  extends: .cki_tools
  script:
    - |
      readarray -t allowed_hosts < <(sed '/^#/d' .allowed-hosts)
      readarray -t internal_hosts < <(grep -hroE --exclude=.allowed-hosts '([a-zA-Z0-9\\.\\-]+\.redhat.com)' * | sort -u)
      fail=0
      for host in "${internal_hosts[@]}"; do
        allowed=0
        for allowed_host in "${allowed_hosts[@]}"; do
          if grep -E -w -q "${allowed_host}" <<< "${host}"; then
              allowed=1
              break
          fi
        done
        if [[ "${allowed}" -eq 0 ]]; then
            echo "${host} is not allowed according to .allowed-hosts"
            fail=1
        fi
      done
      # check if allowed entry should be removed as it is not being used
      for allowed_host in "${allowed_hosts[@]}"; do
        if ! grep -hroEq --exclude=.allowed-hosts "${allowed_host}"; then
            echo "${allowed_host} from .allowed-hosts should be removed as it is not used"
            fail=1
        fi
      done
      exit ${fail}
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

check if LTP version on DCI matches the default on distribution/ltp/include/ltp-make.sh:
  extends: .cki_tools
  script:
    - |
      # Check the DCI test versions
      for rhel_version in 7 8; do
          function rlIsRHEL() { [[ "$1" = "${rhel_version}" ]]; }; source distribution/ltp/include/ltp-make.sh
          dci_xml="dci/rhel${rhel_version}.xml"
          if ! grep -q ltp-full-${TESTVERSION}.tar.bz2 "${dci_xml}"; then
              echo "FAIL: needs to update LTP version on ${dci_xml} to ${TESTVERSION}"
              exit 1
          fi
      done
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

.git-sync:
  extends: .cki_tools
  stage: 🚀
  variables:
    GIT_DEPTH: 100  # works for MRs <= 100 commits on top of the target branch
    FOOTER_CONFIG: |
      name: git-sync
      source_code: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/blob/main/.gitlab-ci.yml
      faq_name: CKI FAQ
      faq_url: https://cki-project.org/devel-faq/
      slack_name: '#team-kernel-cki'
      slack_url: https://redhat-internal.slack.com/archives/C04KPCFGDTN
      new_issue_url: 'https://gitlab.com/cki-project/cki-tools/-/issues/new?issue[title]=kernel-tests git-sync issue'
    COMMENT_TEMPLATE_J2: |
      {% set suite_url = 'https://' +
        env['INTERNAL_GITLAB_HOST'] +
        '/api/v4/projects/' +
        (env['INTERNAL_GITLAB_PROJECT_ID']) +
        '/repository/archive.zip?sha=refs/heads/' +
        env['TARGET_BRANCH']
      -%}
      {% set mr_url = env['CI_MERGE_REQUEST_PROJECT_URL'] +
        '/-/merge_requests/' +
        env['CI_MERGE_REQUEST_IID']
      -%}
      The updated code is synced to the [internal mirror]({{ suite_url }}).

      <details>
      <summary>Click here for an example on how to trigger a Beaker job with it.</summary>

      ```xml
      <job>
        <whiteboard>Beaker job for {{ mr_url }}</whiteboard>
        <recipeSet>
          <recipe ks_meta="redhat_ca_cert">
            <distroRequires>
              <distro_arch op="=" value="x86_64"/>
              <variant op="=" value="BaseOS"/>
              <distro_family op="=" value="CentOSStream9"/>
            </distroRequires>
            <hostRequires/>
            <task name="/test/misc/machineinfo">
              <fetch url="{{ suite_url }}#test/misc/machineinfo"/>
              <params/>
            </task>
          </recipe>
        </recipeSet>
      </job>
      ```
      </details>
  script:
    - git push --force
      "https://git:${GITLAB_JOB_MIRROR_KERNEL_TESTS_MIRROR_ACCESS_TOKEN}@${INTERNAL_GITLAB_HOST}/${INTERNAL_GITLAB_PROJECT}.git"
      "HEAD:refs/heads/$TARGET_BRANCH"
    - |
      if [[ -n "${CI_MERGE_REQUEST_PROJECT_ID:-}" ]]; then
        # delete old bot notes
        bot_user_id=$(
            gitlab \
                --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
                --output json \
                --fields id \
                current-user get \
            | jq \
                --raw-output \
                '.id'
        )
        gitlab \
            --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
            --output json \
            --fields id,author \
            project-merge-request-note list \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --get-all \
        | jq \
            --raw-output \
            --arg user_id "${bot_user_id}" \
            '.[] | select(.author.id == ($user_id | tonumber)) | .id' \
        | xargs \
            --max-args 1 \
            --no-run-if-empty \
            gitlab \
            --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
            project-merge-request-note delete \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --id
        # post a new note with the URL
        body=$(python3 -m cki.deployment_tools.render <<< "${COMMENT_TEMPLATE_J2}")
        footer=$(python3 -m cki_lib.footer --format gitlab)
        python3 -m cki_lib.gitlab \
            --gitlab-url "${CI_SERVER_URL}" \
            --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
            --graphql-query '
              mutation($id: NoteableID!, $body: String!) {
                createNote(input: {noteableId: $id, body:$body, internal: true})
                { errors }
              }' \
            --variables "id=gid://gitlab/MergeRequest/${CI_MERGE_REQUEST_ID}" \
            --variables "body=$(jq -Rs . <<< "${body}${footer}")"
      fi

git-sync-mr:
  extends: .git-sync
  variables:
    TARGET_BRANCH: merge-requests/$CI_MERGE_REQUEST_IID
  rules:
    - !reference [.rules, no-merge-train]
    - !reference [.rules, mr-parent]

git-sync-branches:
  extends: .git-sync
  variables:
    TARGET_BRANCH: $CI_COMMIT_BRANCH
  rules:
    - !reference [.rules, default-branch]
    - !reference [.rules, production-branch]

deploy-production:
  extends: .deploy_production_tag
